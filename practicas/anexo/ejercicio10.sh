#!/bin/bash

touch solo-archi
if [ ! -z $1 ]; then

	if [ -d $1 ]; then

		if [ $(ls $1 | wc -l) -eq 0 ]; then
			echo 'el directorio esta en blanco'
		else
			for i in $(ls $1)
			do
				echo $i >>solo-archi
				contador=$(($contador+1))
			done
		echo $contador >> solo-archi
		fi
	else
		echo 'no es un directorio'
	fi
else
echo 'el parametro no existe'
fi
