#!/bin/bash

if [ ! -z $1 ]; then

	find $1 -type f -perm 755 > perm.txt
	find $1 -type f -name *.conf > exten.txt
fi

