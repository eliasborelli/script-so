#!/bin/bash

CONTADOR=0

for LINEA in $(ls $1)
do
    if [ -f $LINEA ]; then
	CONTADOR=$(($CONTADOR+$(cat $LINEA | wc -l)))
    fi
done
echo $CONTADOR >TOT_GENERAL
