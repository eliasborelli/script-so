#!/bin/bash

if [ ! -z $1 ] && [ -f $1 ]; then

	if [ $(cat $1 | wc -l) -eq 0 ]; then
		echo 'el archivo se paso en blanco'
	else
		echo 'el tamaño es: ' $(du -h $1 | awk '{ print $1 }')	
	fi
fi
