#!/bin/bash

valor1=$(wc -l $1 | cut -d " " -f 1)
valor2=$(wc -l $2 | cut -d " " -f 1)

if [ $valor1 -eq $valor2 ]; then
echo 'Los valores ' $valor1 ' y ' $valor2 ' son iguales.'
else

	if [ $valor1 -gt $valor2 ]; then
		echo 'El ' $valor1 ' es mayor que ' $valor2 ' del archivo ' $1
	else
		echo 'El ' $valor2 ' es mayor que ' $valor1 ' del archivo ' $2
	fi
fi
exit
