#!/bin/bash

existeUsuario=$(cat /etc/passwd | grep -c $1)
existeBash=$(cat /etc/passwd | grep $1 | grep -c bash)

if [ $existeUsuario -gt 0 ]; then

	if [ $existeBash -gt 0 ]; then
		echo ' Existe el usuario y tiene el shell BASH'
	else
		imprimir=$(cat /etc/passwd | grep $1 | awk -F ":" '{ print $7 }')
		echo $imprimir
	fi
fi

