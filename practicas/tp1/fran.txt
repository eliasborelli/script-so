# /etc/fstab: static file system information.
#
# Use 'blkid' to print the universally unique identifier for a
# device; this may be used with UUID= as a more robust way to name devices
# that works even if disks are added and removed. See fstab(5).
#
# <file system> <mount point>   <type>  <options>       <dump>  <pass>
# / was on /dev/sda1 during installation
UUID=e52786f3-46ae-40a2-8b1b-1e9d1432f889 /               ext4    errors=remount-ro 0       1
# swap was on /dev/sda5 during installation
UUID=32745e16-083d-4030-a53c-8c429f2827a9 none            swap    sw              0       0
/dev/sr0        /media/cdrom0   udf,iso9660 user,noauto     0       0
