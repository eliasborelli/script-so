#!/bin/bash

directorio1=$(du $1 | awk '{ print $1 }')
directorio2=$(du $2 | awk '{ print $1 }')

if [ $directorio1 -eq $directorio2 ]; then

	echo 'Los directorios son iguales'

else

	if [ $directorio1 -gt $directorio2 ]; then

		echo 'el directorio $1 es mayor que $2'
	else

		echo 'el directorio $2 es mayor que $1'
	fi

fi

