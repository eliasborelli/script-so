#!/bin/bash

if [ ! -z $1 ] && [ ! -z $2 ]; then
echo 'Existen ambos archivos'

	if [ -d $1 ] && [ -f $2 ]; then
	echo 'Ambos parametros estan validados'


		if [ $(ls $1 | wc -l) -eq 0 ]; then
		cp $2 $1
		fi
	else
	echo 'Alguno de los dos parametros no pudieron ser validados'
	fi
else
echo 'Alguno de los dos parametros no existe...'
fi

